package services;

public class Loggeur {
	
	public void methodeAvant()
	{
		System.out.println("Le loggeur est appelé");
	}
	
	public void methodeApres(String result)
	{
		System.out.println("Le texte --"+result+"-- a été envoyé au loggeur");
	}

}

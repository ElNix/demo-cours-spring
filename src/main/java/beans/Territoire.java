package beans;

public class Territoire {
	
	private String nom;
	private int superficie;
	
	public Territoire(String nom, int superficie) {
		super();
		this.nom = nom;
		this.superficie = superficie;
	}
	
	public Territoire() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getSuperficie() {
		return superficie;
	}

	public void setSuperficie(int superficie) {
		this.superficie = superficie;
	}

	@Override
	public String toString() {
		return "Territoire [nom=" + nom + ", superficie=" + superficie + "]";
	}
	
	
	

}

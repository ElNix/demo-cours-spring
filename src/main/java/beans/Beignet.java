package beans;

public class Beignet {
	
	private int diametre;
	private String farine;
	private String topping;
	
	public Beignet() {
		super();
	}

	public Beignet(int diametre, String farine, String topping) {
		super();
		this.diametre = diametre;
		this.farine = farine;
		this.topping = topping;
	}

	@Override
	public String toString() {
		return "Beignet de " + diametre + "cm de diametre à la farine " + farine + " et un topping " + topping + ".";
	}
	
	public int getDiametre() {
		return diametre;
	}
	public void setDiametre(int diametre) {
		this.diametre = diametre;
	}
	public String getFarine() {
		return farine;
	}
	public void setFarine(String farine) {
		this.farine = farine;
	}
	public String getTopping() {
		return topping;
	}
	public void setTopping(String topping) {
		this.topping = topping;
	}
	
	
	

}

package beans;

public class TestSingleton {

	private String valeurInit = "valeur de base";
	private String valeurNonInit;
	
	public TestSingleton() {
		// TODO Auto-generated constructor stub
	}

	public String getValeurInit() {
		return valeurInit;
	}

	public void setValeurInit(String valeurInit) {
		this.valeurInit = valeurInit;
	}

	public String getValeurNonInit() {
		return valeurNonInit;
	}

	public void setValeurNonInit(String valeurNonInit) {
		this.valeurNonInit = valeurNonInit;
	}

	@Override
	public String toString() {
		return "TestSingleton [valeurInit=" + valeurInit + ", valeurNonInit=" + valeurNonInit + "]";
	}
	
	

}

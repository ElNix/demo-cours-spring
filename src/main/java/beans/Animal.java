package beans;

public class Animal {
	
	private String nom;
	private String espece;
	private int age;
	private Territoire terr;
	
	public Animal(String nom, String espece, int age, Territoire territoire) {
		super();
		this.nom = nom;
		this.espece = espece;
		this.age = age;
		this.terr = territoire;
	}
	
	public Animal() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Territoire getTerr() {
		return terr;
	}

	public void setTerr(Territoire territoire) {
		this.terr = territoire;
	}

	@Override
	public String toString() {
		return "Animal [nom=" + nom + ", espece=" + espece + ", age=" + age + ", territoire=" + terr + "]";
	}
	
	

}

	
	package beans;
	
	public class FabriqueBeignets {
		
		public Beignet getBeignet(String commande)
		{
			Beignet beignet;
			
			if(commande.equals("banane"))
				beignet = new Beignet(10,"blanche","coulis de banane");
			else if(commande.equals("doublechoco"))
				beignet = new Beignet(15,"au chocolat noir","pépites de chocolat");
			else if(commande.equals("fraise"))
				beignet = new Beignet(8,"blanche","coulis de fraise");
			else
				beignet = new Beignet(0,"ERREUR","ERREUR");
			
			return beignet;
		}
	
	}



//pouet
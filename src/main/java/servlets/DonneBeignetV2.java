

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Beignet;
import beans.FabriqueBeignets;

/**
 * Servlet implementation class DonneCookie
 */
public class DonneBeignetV2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* Méthode doGet ou do Post*/
		
		//Récupération de la demande
		String commande = request.getParameter("commande");
		
		//Préparation de l'instance en fonction de la demande
		FabriqueBeignets fab = new FabriqueBeignets();
		Beignet beignet = fab.getBeignet(commande);
		
		//Envoie de l'instance
		request.setAttribute( "beignet", beignet );
		
		/* Appel à la JSP */
		
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/donneBeignet.jsp" ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

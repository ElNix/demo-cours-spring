

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.FabriqueBeignets;
import beans.TestSingleton;
import services.StockAppli;

/**
 * Servlet implementation class TestSpringSingleton1
 */
public class TestSpringSingleton1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestSpringSingleton1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring-context.xml");

		ApplicationContext context = StockAppli.context;
		
		TestSingleton ts = context.getBean(TestSingleton.class);
		
		response.getWriter().append("Avant: ").append(ts.toString()).append("<br/>");
		
		ts.setValeurInit("nouvelle init");
		ts.setValeurNonInit("nouvelle non init");
		
		response.getWriter().append("Apres: ").append(ts.toString()).append("<br/>");
		
		TestSingleton ts2 = context.getBean(TestSingleton.class);

		response.getWriter().append("New recup: ").append(ts2.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

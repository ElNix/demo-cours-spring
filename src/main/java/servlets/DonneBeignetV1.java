

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Beignet;

/**
 * Servlet implementation class DonneCookie
 */
public class DonneBeignetV1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DonneBeignetV1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* Méthode doGet ou do Post*/
		
		//Récupération de la demande
		String commande = request.getParameter("commande");
		
		Beignet beignet;
		
		//Préparation de l'instance en fonction de la demande
		if(commande.equals("banane"))
			beignet = new Beignet(10,"blanche","coulis de banane");
		else if(commande.equals("doublechoco"))
			beignet = new Beignet(15,"au chocolat noir","pépites de chocolat");
		else if(commande.equals("fraise"))
			beignet = new Beignet(8,"blanche","coulis de fraise");
		else
			beignet = new Beignet(0,"ERREUR","ERREUR");
		
		//Envoie de l'instance
		request.setAttribute( "beignet", beignet );
		
		/* Appel à la JSP */
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/donneBeignet.jsp" ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

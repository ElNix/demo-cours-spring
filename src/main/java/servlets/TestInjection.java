

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Animal;
import beans.Territoire;

/**
 * Servlet implementation class TestInjection
 */
public class TestInjection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestInjection() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring-context.xml");
		
		Territoire terr1 = context.getBean("territoireParSetters", Territoire.class);
		System.out.println(terr1);
		
		Territoire terr2 = context.getBean("territoireParConstructeur", Territoire.class);
		System.out.println(terr2);
		
		Animal sammy = context.getBean("animalAvecTerritoire", Animal.class);
		System.out.println(sammy);
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		Territoire territoireParSetters = new Territoire();
		territoireParSetters.setNom("Toundra");
		territoireParSetters.setSuperficie(150000);
		
		Territoire territoireParConstructeur = new Territoire("Foret Boreale",75000);
		
		Animal animalAvecTerritoire = new Animal();
		animalAvecTerritoire.setNom("Sammy");
		animalAvecTerritoire.setEspece("Renard Artique");
		animalAvecTerritoire.setAge(14);
		animalAvecTerritoire.setTerr(territoireParConstructeur);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

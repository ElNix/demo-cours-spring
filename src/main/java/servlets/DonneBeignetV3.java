

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Beignet;
import beans.FabriqueBeignets;

/**
 * Servlet implementation class DonneBeignetV3
 */
public class DonneBeignetV3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DonneBeignetV3() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	@SuppressWarnings("resource")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* Méthode doGet ou do Post*/
		
		//Préparation de la récupération Spring
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring-context.xml");
		
		//Récupération de la fabrique à partir de la configuration Spring
		FabriqueBeignets fab = context.getBean("fabriqueBeignets", FabriqueBeignets.class);
		
		
		//Récupération de la demande et réparation de l'instance en fonction de la demande
		String commande = request.getParameter("commande");
		Beignet beignet = fab.getBeignet(commande);
		
		//Envoie de l'instance
		request.setAttribute( "beignet", beignet );
		
		/* Appel à la JSP */
		
		System.out.println("V3");
		this.getServletContext().getRequestDispatcher( "/WEB-INF/donneBeignet.jsp" ).forward( request, response );

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
